/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <modal_json.h>
#include <modal_pipe_client.h>

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration that's specific to voxl-qvio-server.\n\
 *\n\
 * voxl-qvio-server also uses parameters from the following config files:\n\
 * /etc/modalai/extrinsics.conf\n\
 * /data/modalai/opencv_tracking_intrinsics.yml\n\
 *\n\
 * parameter descriptions:\n\
 * \n\
 * imu_name: VOXL uses imu1 by defualt since it's the most reliable. On\n\
 * VOXL-FLIGHT you can optionally try imu0 which is a newer icm42688\n\
 * voxl-configure-mpa will decide which IMU to use for you.\n\
 *\n\
 * cam_name: camera to use, defaults to tracking0\n\
 *\n\
 * odr_hz: Output data date is independent from the camera frame rate so you can\n\
 * choose the desired output data rate. Note that voxl-imu-server defaults to\n\
 * 500hz imu sampling but new data is received by voxl-qvio-server at 100hz by\n\
 * default so requesting qvio data faster requires updating voxl-imu-server.\n\
 *\n\
 * use_camera_height_bootstrap: When enabled, the feature estimator will assume\n\
 * the system starts up with the camera pointed partially at the ground and use\n\
 * this to make an intial guess of the feature's depth. This should be enabled\n\
 * for drones that have known-height landing gear.\n\
 *\n\
 * camera_height_off_ground_m: camera distance above ground (meters) for the\n\
 * above bootstrap feature.\n\
 *\n\
 * enable_init_while_moving: optionally allow the algorithm to initialize or\n\
 * reinitialize while moving. Use this if you want to be able to reinitialize\n\
 * during flight. Based on camera motion, not IMU motion. Recommended to leave\n\
 * this off unless absolutely desired.\n\
 *\n\
 * cam_imu_timeshift_s: Misalignment between camera and imu timestamp in\n\
 * seconds.\n\
 *\n\
 * cam_imu_timeshift_s_uncertainty: uncertainty in camera imu timestamp\n\
 * misalignment\n\
 *\n\
 * T_cam_wrt_imu_uncertainty[3] & R_cam_to_imu_uncertainty[3]: uncertainty in\n\
 * camera-imu translation\n\
 *\n\
 * accl_fsr_ms2 & gyro_fsr_rad: Full scale range used to detect clipping. By\n\
 * default this is set to a little under the real 16G and 2000DPS FSR so\n\
 * clipping is detected reliably\n\
 *\n\
 * accl_noise_std_dev & gyro_noise_std_dev: standard deviation of accl and gyro\n\
 * noise\n\
 *\n\
 * cam_noise_std_dev: Standard dev of camera noise per pixel.\n\
 *\n\
 * min_std_pixel_noise: Minimum of standard deviation of feature measurement\n\
 * noise in pixels.\n\
 *\n\
 * fail_high_pixel_noise_points: Scales measurement noise and compares against\n\
 * search area (is search area large enough to reliably compute measurement\n\
 * noise covariance matrix).\n\
 *\n\
 * limited_imu_bw_trigger: To prevent tracking failure during/right after (hard)\n\
 * landing: If sum of 3 consecutive accelerometer samples in any dimension\n\
 * divided by 4.3 exceed this threshold, IMU measurement noise is increased (and\n\
 * resets become more likely); if platform vibrates heavily during flight, this\n\
 * may trigger mid- flight; if poseQuality in mvVISLAMPose drops to\n\
 * MV_TRACKING_STATE_LOW_QUALITY during flight, improve mechanical dampening\n\
 * (and/or increase threshold). QC default is 35, we prefer 25.\n\
 *\n\
 * gps_imu_time_alignment_s: Misalignment between GPS and IMU time in seconds\n\
 *\n\
 * T_gps_wrt_imu: location of gps with respect to the IMU in meters\n\
 *\n\
 * enable_mapping: rudimentary lightweight mapping of feature points, leave this\n\
 * on.\n\
 *\n\
 * enable_gps_vel: allow gps velocity to be added to VIO, requires ModalAI\n\
 * custom PX4 firmware to expose this data. This is an Alpha feature, don't\n\
 * enable this unless instructed to.\n\
 *\n\
 *\n\
 *\n\
 * en_auto_reset: on by default. If any one of the following thresholds are met\n\
 * voxl-qvio-server will assume an otherwise undetected fault has occured in\n\
 * the inderlying algorithm and will hard reset the QVIO library.\n\
 *\n\
 * auto_reset_max_velocity: meters per second, if velocity faster than this is\n\
 * detected then an auto-reset is triggered\n\
 *\n\
 * auto_reset_max_v_cov_instant: if the covariance of the velocity estimate ever\n\
 * exceeds this for a since sample then an auto-reset is triggered\n\
 *\n\
 * auto_reset_max_v_cov: if th covariance of the velocity estimate ever exceeds\n\
 * this for more than auto_reset_max_v_cov_timeout_s then an auto-reset is triggered\n\
 *\n\
 * auto_reset_max_v_cov_timeout_s: time period for auto_reset_max_v_cov criteria\n\
 *\n\
 * auto_reset_min_features: if fewer than this number of features are tracked for\n\
 * more than auto_reset_min_feature_timeout_s then an auto-reset is triggered.\n\
 * it is okay for feature tracking to be lost for short periods of time.\n\
 *\n\
 *\n\
 *\n\
 * en_standby_mode: When enabled, qvio will skip camera frames while voxl-cpu-monitor\n\
 *                  reports that the CPU is in standby mode.\n\
 *\n\
 * standby_skip_frames: number of frames to skip before processing one. default is 1,\n\
                        meaning skip every other frame. 30 -> 15hz\n\
 */\n"

static char imu_name[MODAL_PIPE_MAX_NAME_LEN];	// pipe name read from config file
static char cam_name[MODAL_PIPE_MAX_NAME_LEN];	// pipe name read from config file
// static char imu_pipe_location[MODAL_PIPE_MAX_DIR_LEN];  // full pipe directory constructed from temp name
// static char cam_pipe_location[MODAL_PIPE_MAX_DIR_LEN];  // full pipe directory constructed from temp name
static char lens_cal_path[MODAL_PIPE_MAX_PATH_LEN];
static char cam_extrinsics_name[MODAL_PIPE_MAX_NAME_LEN];
static int use_second_cam_if_stereo; // When reading a stereo pair camera pipe, use the second of the 2 images
static float odr_hz;
static int use_camera_height_bootstrap;
static float camera_height_off_ground_m;
static int enable_init_while_moving;
static float cam_imu_timeshift_s;
static float cam_imu_timeshift_s_uncertainty;
static float T_cam_wrt_imu_uncertainty[3];
static float R_cam_to_imu_uncertainty[3];
static float accl_fsr_ms2;
static float gyro_fsr_rad;
static float accl_noise_std_dev;
static float gyro_noise_std_dev;
static float cam_noise_std_dev;
static float min_std_pixel_noise;
static float fail_high_pixel_noise_points;
static float limited_imu_bw_trigger;
static float gps_imu_time_alignment_s;
static float T_gps_wrt_imu[3];
static int enable_mapping;
static int enable_gps_vel;

// auto reset parameters
static int en_auto_reset;
static float auto_reset_max_velocity;
static float auto_reset_max_v_cov_instant;
static float auto_reset_max_v_cov;
static float auto_reset_max_v_cov_timeout_s;
static int   auto_reset_min_features;
static float auto_reset_min_feature_timeout_s;

// standby params
static int en_standby_mode;
static int standby_skip_frames;

static float default_T_cam_wrt_imu_uncertainty[] = {0.0005f, 0.0005f, 0.0005f};
static float default_R_cam_to_imu_uncertainty[] = {0.005f, 0.005f, 0.005f};
static float default_T_gps_wrt_imu[] = {0.0f, 0.0f, 0.0f};

/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static void config_file_print(void)
{
	printf("=================================================================\n");
	printf("imu_name:                         %s\n",    imu_name);
	printf("cam_name:                         %s\n",    cam_name);
	printf("lens_cal_path:                    %s\n",    lens_cal_path);
	printf("cam_extrinsics_name:              %s\n",    cam_extrinsics_name);
	printf("use_second_cam_if_stereo:         %d\n",    use_second_cam_if_stereo);
	printf("odr_hz:                           %6.3f\n", (double)odr_hz);
	printf("use_camera_height_bootstrap:      %d\n",    use_camera_height_bootstrap);
	printf("camera_height_off_ground_m:       %6.3f\n", (double)camera_height_off_ground_m);
	printf("enable_init_while_moving:         %d\n",    enable_init_while_moving);
	printf("cam_imu_timeshift_s:              %6.3f\n", (double)cam_imu_timeshift_s);
	printf("cam_imu_timeshift_s_uncertainty:  %6.3f\n", (double)cam_imu_timeshift_s_uncertainty);
	printf("T_cam_wrt_imu_uncertainty:        %6.3f %6.3f %6.3f\n", (double)T_cam_wrt_imu_uncertainty[0], (double)T_cam_wrt_imu_uncertainty[1], (double)T_cam_wrt_imu_uncertainty[2]);
	printf("R_cam_to_imu_uncertainty:         %6.3f %6.3f %6.3f\n", (double)R_cam_to_imu_uncertainty[0],  (double)R_cam_to_imu_uncertainty[1],  (double)R_cam_to_imu_uncertainty[2]);
	printf("accl_fsr_ms2:                     %6.3f\n", (double)accl_fsr_ms2);
	printf("gyro_fsr_rad:                     %6.3f\n", (double)gyro_fsr_rad);
	printf("accl_noise_std_dev:               %6.3f\n", (double)accl_noise_std_dev);
	printf("gyro_noise_std_dev:               %6.3f\n", (double)gyro_noise_std_dev);
	printf("cam_noise_std_dev:                %6.3f\n", (double)cam_noise_std_dev);
	printf("min_std_pixel_noise:              %6.3f\n", (double)min_std_pixel_noise);
	printf("fail_high_pixel_noise_points:     %6.3f\n", (double)fail_high_pixel_noise_points);
	printf("limited_imu_bw_trigger:           %6.3f\n", (double)limited_imu_bw_trigger);
	printf("gps_imu_time_alignment_s:         %6.3f\n", (double)gps_imu_time_alignment_s);
	printf("T_gps_wrt_imu:                    %6.3f %6.3f %6.3f\n", (double)T_gps_wrt_imu[0], (double)T_gps_wrt_imu[1], (double)T_gps_wrt_imu[2]);
	printf("enable_mapping:                   %d\n",    enable_mapping);
	printf("enable_gps_vel:                   %d\n",    enable_gps_vel);
	printf("auto-reset params:\n");
	printf("en_auto_reset:                    %d\n",    en_auto_reset);
	printf("auto_reset_max_velocity:          %6.3f\n", (double)auto_reset_max_velocity);
	printf("auto_reset_max_v_cov_instant:     %6.3f\n", (double)auto_reset_max_v_cov_instant);
	printf("auto_reset_max_v_cov:             %6.3f\n", (double)auto_reset_max_v_cov);
	printf("auto_reset_max_v_cov_timeout_s:   %6.3f\n", (double)auto_reset_max_v_cov_timeout_s);
	printf("auto_reset_min_features:          %d\n",    auto_reset_min_features);
	printf("auto_reset_min_feature_timeout_s: %6.3f\n", (double)auto_reset_min_feature_timeout_s);
	printf("standby params:\n");
	printf("en_standby_mode:                  %d\n",    en_standby_mode);
	printf("standby_skip_frames:              %d\n",    standby_skip_frames);
	printf("=================================================================\n");
	return;
}


/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(VOXL_QVIO_SERVER_CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", VOXL_QVIO_SERVER_CONF_FILE);

	cJSON* parent = json_read_file(VOXL_QVIO_SERVER_CONF_FILE);
	if(parent==NULL) return -1;

	json_fetch_string_with_default(	parent, "imu_name",						imu_name, MODAL_PIPE_MAX_NAME_LEN,	"imu0");
	json_fetch_string_with_default(	parent, "cam_name",						cam_name, MODAL_PIPE_MAX_NAME_LEN,	"tracking");
	json_fetch_string_with_default(	parent, "lens_cal_path",				lens_cal_path, MODAL_PIPE_MAX_PATH_LEN,	"/data/modalai/opencv_tracking_intrinsics.yml");
	json_fetch_string_with_default(	parent, "cam_extrinsics_name",			cam_extrinsics_name, MODAL_PIPE_MAX_NAME_LEN,	"tracking");
	json_fetch_bool_with_default(	parent, "use_second_cam_if_stereo",		&use_second_cam_if_stereo,		0);
	json_fetch_float_with_default(	parent, "odr_hz",						&odr_hz,						30);
	json_fetch_bool_with_default(	parent, "use_camera_height_bootstrap",	&use_camera_height_bootstrap,	1);
	json_fetch_float_with_default(	parent, "camera_height_off_ground_m",	&camera_height_off_ground_m,	0.085);
	json_fetch_bool_with_default(	parent, "enable_init_while_moving",		&enable_init_while_moving,		0);
	json_fetch_float_with_default(	parent, "cam_imu_timeshift_s",			&cam_imu_timeshift_s,			-0.002);
	json_fetch_float_with_default(	parent, "cam_imu_timeshift_s_uncertainty",		&cam_imu_timeshift_s_uncertainty,0.001);
	json_fetch_fixed_vector_float_with_default(parent, "T_cam_wrt_imu_uncertainty",	T_cam_wrt_imu_uncertainty,	3,	default_T_cam_wrt_imu_uncertainty);
	json_fetch_fixed_vector_float_with_default(parent, "R_cam_to_imu_uncertainty",	R_cam_to_imu_uncertainty,	3,	default_R_cam_to_imu_uncertainty);
	json_fetch_float_with_default(	parent, "accl_fsr_ms2",					&accl_fsr_ms2,					156.0);
	json_fetch_float_with_default(	parent, "gyro_fsr_rad",					&gyro_fsr_rad,					34.0);
	json_fetch_float_with_default(	parent, "accl_noise_std_dev",			&accl_noise_std_dev,			0.316);
	json_fetch_float_with_default(	parent, "gyro_noise_std_dev",			&gyro_noise_std_dev,			0.01);
	json_fetch_float_with_default(	parent, "cam_noise_std_dev",			&cam_noise_std_dev,				100.0);
	json_fetch_float_with_default(	parent, "min_std_pixel_noise",			&min_std_pixel_noise,			0.5);
	json_fetch_float_with_default(	parent, "fail_high_pixel_noise_points",	&fail_high_pixel_noise_points,	1.6651f);
	json_fetch_float_with_default(	parent, "limited_imu_bw_trigger",		&limited_imu_bw_trigger,		25);
	json_fetch_float_with_default(	parent, "gps_imu_time_alignment_s",		&gps_imu_time_alignment_s,		0.0);
	json_fetch_fixed_vector_float_with_default(parent, "T_gps_wrt_imu",		T_gps_wrt_imu,	3,	default_T_gps_wrt_imu);
	json_fetch_bool_with_default(	parent, "enable_mapping",				&enable_mapping,				1);
	json_fetch_bool_with_default(	parent, "enable_gps_vel",				&enable_gps_vel,				0);

	// auto reset features
	json_fetch_bool_with_default(	parent, "en_auto_reset",				&en_auto_reset,					1);
	json_fetch_float_with_default(	parent, "auto_reset_max_velocity",		&auto_reset_max_velocity,		10.0f);
	json_fetch_float_with_default(	parent, "auto_reset_max_v_cov_instant",	&auto_reset_max_v_cov_instant,	0.1f);
	json_fetch_float_with_default(	parent, "auto_reset_max_v_cov",			&auto_reset_max_v_cov,			0.01f);
	json_fetch_float_with_default(	parent, "auto_reset_max_v_cov_timeout_s",&auto_reset_max_v_cov_timeout_s,0.5f);
	json_fetch_int_with_default(	parent, "auto_reset_min_features",		&auto_reset_min_features,		3);
	json_fetch_float_with_default(	parent, "auto_reset_min_feature_timeout_s",&auto_reset_min_feature_timeout_s,1.0f);

	// standby settings
	json_fetch_bool_with_default(	parent, "en_standby_mode",			&en_standby_mode,			1);
	json_fetch_int_with_default(	parent, "standby_skip_frames",		&standby_skip_frames,		1);


	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", VOXL_QVIO_SERVER_CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// no need for this field anymore
	json_remove_if_present(parent, "warmup_frames");
	json_remove_if_present(parent, "show_extra_points_on_overlay");

	// // construct proper full pipe paths from the provided imu and cam names
	// if(pipe_expand_location_string(imu_name, imu_pipe_location)<0){
	// 	fprintf(stderr, "Invalid imu pipe name: %s\n", imu_name);
	// 	return -1;
	// }
	// if(pipe_expand_location_string(cam_name, cam_pipe_location)<0){
	// 	fprintf(stderr, "Invalid cam pipe name: %s\n", cam_name);
	// 	return -1;
	// }


	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		//printf("The config file was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(VOXL_QVIO_SERVER_CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}

#endif // end #define CONFIG_FILE_H
