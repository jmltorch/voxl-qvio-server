/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with hardware devices provided by
 *    ModalAI® Inc. Reverse engineering or copying this code for use on hardware 
 *    not manufactured by ModalAI is not permitted.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <getopt.h>
#include <sched.h>

#include <mvVISLAM.h>

#include <modal_pipe.h>
#include <voxl_common_config.h>
#include <cpu_monitor_interface.h>

#include <voxl_qvio_server.h>
#include "time.h"
#include "config_file.h"
#include "quality.h"
#include "overlay.h"

// server channels
#define EXTENDED_CH	0
#define SIMPLE_CH	1
#define OVERLAY_CH	2

// client channels and config
#define IMU_CH	0
#define CAM_CH	1
#define GPS_CH	2
#define CPU_CH	3
#define IMU_PIPE_MIN_PIPE_SIZE (1  * 1024 * 1024) // give ourselves huge buffers
#define CAM_PIPE_SIZE (256 * 1024 * 1024) // give ourselves huge buffers
#define PROCESS_NAME	"qvio-server"

#define GPS_PIPE_NAME	"vvpx4_vehicle_gps"

// after 300ms with no response, the health monitor thread assumes mvVISLAM
// has locked up while processing a frame and starts sending messages indicating
// a stall has occured with a failed state
#define STALL_TIMEOUT_NS 300000000

// auto restart if the system fails to init after 2 seconds
#define INIT_FAILURE_TIMEOUT_NS 2000000000

// do not check for blowups until 1 second after VIO claims to have initialized
#define BLOWUP_DETECT_TIMEOUT_NS 1000000000


#define SILENT_STD(x) {\
	FILE *silentfd = fopen("/dev/null","w");     \
	int savedstdoutfd = dup(STDOUT_FILENO);      \
	fflush(stdout);                              \
	dup2(fileno(silentfd), STDOUT_FILENO);       \
	x                                            \
	fflush(stdout);                              \
	fclose(silentfd);                            \
	dup2(savedstdoutfd, STDOUT_FILENO);          \
	close(savedstdoutfd);                        \
}


static int en_config_only = 0;
static int en_debug = 0;
static int en_debug_pos = 0;
static int en_debug_timing_cam = 0;
static int en_debug_timing_imu = 0;
static int en_debug_timing_pose = 0;
static int en_debug_crash = 0;
static int en_debug_gps = 0;
static pthread_t health_thread;
static int standby_active = 0;

// these are the last timestamps that have completely passed into mvvislam
// cam time is middle of frame. Also last pose to have been received from mvvislam
static volatile int64_t last_imu_timestamp_ns = 0;
static volatile int64_t last_cam_timestamp_ns = 0;
static volatile int64_t last_real_pose_timestamp_ns = 0;
static volatile int64_t last_sent_timestamp_ns = 0;

// state of imu and camera connections
static volatile int is_imu_connected = 0;
static volatile int is_cam_connected = 0;

// flag set to 1 on reset to indicate to the blowup detector not to check
// for blowups until after VIO actually initializes
static volatile int hard_reset_blowup_flag = 1;

// flag and time when a reset is requested to indicate to the init failure
// detection how long VIO has been trying to init
static volatile int init_failure_detector_reset_flag = 1;
static volatile int64_t time_of_last_reset = 0;
static volatile int last_state = VIO_STATE_FAILED;
static volatile int blowup_detector_flag = 0;
static volatile int64_t time_of_first_okay = 0;


static mvVISLAM* mv_vislam_ptr;
static int is_initialized = 0;
static int blank_counter = 0;
static int fade_counter = 0;
static int64_t last_time_alignment_ns = 0;
static int32_t last_frame_frame_id = 0;
static int64_t last_frame_timestamp_ns = 0;
static float tbc[3];
static float ombc[3];
static mvCameraConfiguration mv_cam_conf;

// mvVISLAM functions are not thread safe, protect all calls to the mvVISLAM
// API with this mutex
static pthread_mutex_t mv_mtx = PTHREAD_MUTEX_INITIALIZER;

// set any error codes here for publishing in the data structure in addition
// to errors that come from mvVISLAM
static uint32_t global_error_codes = 0;

// function prototypes
static void _publish(camera_image_metadata_t meta, uint8_t* img);
static int _hard_reset(void);


// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-c, --config                only parse the config file and exit, don't run\n\
-d, --debug                 enable debug prints\n\
-g, --debug-gps             enable gps-specific debug prints\n\
-h, --help                  print this help message\n\
-i, --timing-imu            show timing prints for imu processing\n\
-p, --position              print position and rotation\n\
-s, --debug-crash           print lots of numbers to track down location of crashes\n\
-t, --timing-cam            enable timing prints for camera processing\n\
-u, --timing-pose           enable timing prints for pose calculation\n\
\n");
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",			no_argument,		0, 'c'},
		{"debug",			no_argument,		0, 'd'},
		{"debug-gps",		no_argument,		0, 'g'},
		{"help",			no_argument,		0, 'h'},
		{"timing-imu",		no_argument,		0, 'i'},
		{"position",		no_argument,		0, 'p'},
		{"debug-crash",		no_argument,		0, 's'},
		{"timing-cam",		no_argument,		0, 't'},
		{"timing-pose",		no_argument,		0, 'u'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdghipstu", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			en_config_only = 1;
			break;

		case 'd':
			en_debug = 1;
			break;

		case 'g':
			en_debug_gps = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 'i':
			en_debug_timing_imu = 1;
			break;

		case 'p':
			en_debug_pos = 1;
			break;

		case 's':
			en_debug_crash = 1;
			break;

		case 't':
			en_debug_timing_cam = 1;
			break;

		case 'u':
			en_debug_timing_pose = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


// call this instead of return when it's time to exit to cleans up everything
static void _quit(int ret)
{
	printf("attempting to shut down cleanly\n");
	pipe_server_close_all();
	pipe_client_close_all();
	if(is_initialized){
		mvVISLAM_Deinitialize(mv_vislam_ptr);
		is_initialized = 0;
	}
	remove_pid_file(PROCESS_NAME);
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


// pose data is published from the same thread that does the camera processing
// and pose estimation. That freezes, sometimes for over a second, during blowups
// so this thread exists to keep data coming out during that situation, warning
// consumers that there is an issue.
// this does NOT monitor for blowup criteria, that's done in the camera thread
// as soon as a new pose is calculated. This thread is to warn when that
// camera thread freezes.
static void* _health_thread_func(__attribute__((unused)) void* ctx)
{
	while(main_running){
		usleep(30000); // run about the same speed as the camera

		int64_t current_time = _apps_time_monotonic_ns();
		int64_t delay_ns = current_time - last_real_pose_timestamp_ns;

		if(init_failure_detector_reset_flag && is_imu_connected && is_cam_connected){
			uint64_t time_since_reset = current_time - time_of_last_reset;
			if(time_since_reset > INIT_FAILURE_TIMEOUT_NS){
				fprintf(stderr, "WARNING failed to init in time, trying again\n");
				_hard_reset();
				continue;
			}
		}

		// If last packet is recent enough, or we are chilling in standby mode
		// there is nothing to worry about.
		if(delay_ns < STALL_TIMEOUT_NS || standby_active) continue;

		// Everything after this sends failure packets, this inlcudes global
		// error codes indicating if we are waiting for cam or IMU data

		// flag that we've sent a packet with the current timestamp
		last_sent_timestamp_ns = current_time;

		ext_vio_data_t d;	// complete "extended" vio MPA packet
		vio_data_t s;		// simplified vio packet

		// simple lib modal pipe standard vio packet
		memset(&s,0,sizeof(s));
		s.magic_number = VIO_MAGIC_NUMBER;
		s.timestamp_ns = current_time;
		s.error_code = global_error_codes | ERROR_CODE_STALLED;
		s.state = VIO_STATE_FAILED;
		s.quality = -1.0f;

		// full extended qvio packet
		memset(&d,0,sizeof(d));
		d.v = s;

		// send to both pipes
		pipe_server_write(EXTENDED_CH, (char*)&d, sizeof(ext_vio_data_t));
		pipe_server_write(SIMPLE_CH,   (char*)&s, sizeof(vio_data_t));
		_draw_and_publish_vio_overlay(OVERLAY_CH, NULL, NULL, d);

		// turn off dropped cam frame code now we have informed everyone.
		global_error_codes &= ~ERROR_CODE_DROPPED_CAM;
	}

	fprintf(stderr, "exiting health thread\n");
	return NULL;
}



static int _hard_reset(void)
{
	// lock the mutex before calling any mv api calls
	pthread_mutex_lock(&mv_mtx);

	// stop it if it's running
	if(is_initialized){
		is_initialized = 0;
		SILENT_STD(mvVISLAM_Deinitialize(mv_vislam_ptr);)
		mv_vislam_ptr = NULL;
	}

	// let the blowup detection know we just had a reset
	hard_reset_blowup_flag = 1;

	// let the init failure detector know we just had a reset
	init_failure_detector_reset_flag = 1;
	blowup_detector_flag = 0;
	last_state = VIO_STATE_FAILED;
	time_of_last_reset = _apps_time_monotonic_ns();

	// constant parameters
	float tba[3] = {0, 0, 0}; // position of gps relative to imu
	char* staticMaskFileName = NULL; // for image mask
	float logDepthBootstrap = log(1.0); // unused depth bootstrap

	// now start again
	SILENT_STD(mv_vislam_ptr = mvVISLAM_Initialize(
		&mv_cam_conf,
		0, // global shutter
		tbc,
		ombc,
		cam_imu_timeshift_s,
		T_cam_wrt_imu_uncertainty,
		R_cam_to_imu_uncertainty,
		cam_imu_timeshift_s_uncertainty,
		accl_fsr_ms2,
		gyro_fsr_rad,
		accl_noise_std_dev,
		gyro_noise_std_dev,
		cam_noise_std_dev,
		min_std_pixel_noise,
		fail_high_pixel_noise_points,
		logDepthBootstrap,
		use_camera_height_bootstrap,
		log(camera_height_off_ground_m),
		!enable_init_while_moving,
		limited_imu_bw_trigger,
		staticMaskFileName,
		gps_imu_time_alignment_s,
		tba,
		enable_mapping);)

	pthread_mutex_unlock(&mv_mtx);

	if(mv_vislam_ptr == NULL){
		fprintf(stderr, "Error creating mvVISLAM object\n");
		_quit(-1);
	}

	is_initialized = 1;

	return 0;
}




// convert roll/pitch/yaw in degrees from extrinsics config file to axis-angle
static int _tait_bryan_xyz_intrinsic_to_axis_angle(double tb_deg[3], float aa[3])
{
	if(tb_deg==NULL||aa==NULL){
		fprintf(stderr,"ERROR: in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// convert from degree to rad
	double tb_rad[3];
	tb_rad[0] = tb_deg[0]*M_PI/180.0;
	tb_rad[1] = tb_deg[1]*M_PI/180.0;
	tb_rad[2] = tb_deg[2]*M_PI/180.0;

	double c1 = cos(tb_rad[0]/2.0);
	double s1 = sin(tb_rad[0]/2.0);
	double c2 = cos(tb_rad[1]/2.0);
	double s2 = sin(tb_rad[1]/2.0);
	double c3 = cos(tb_rad[2]/2.0);
	double s3 = sin(tb_rad[2]/2.0);
	double c1c2 = c1*c2;
	double s1s2 = s1*s2;
	double s1c2 = s1*c2;
	double c1s2 = c1*s2;

	// XYZ
	double w = c1c2*c3 - s1s2*s3;
	double x = s1c2*c3 + c1s2*s3;
	double y = c1s2*c3 - s1c2*s3;
	double z = c1c2*s3 + s1s2*c3;

	double norm = x*x+y*y+z*z;
	if(norm < 0.0001){
		aa[0]=0.0f;
		aa[1]=0.0f;
		aa[2]=0.0f;
		return 0;
	}

	double angle = 2.0*acos(w);
	double scale = angle/sqrt(norm);
	aa[0] = x*scale;
	aa[1] = y*scale;
	aa[2] = z*scale;
	return 0;
}


// control listens for reset commands
static void _control_pipe_cb(__attribute__((unused)) int ch, char* string, \
							 int bytes, __attribute__((unused)) void* context)
{
	// remove the trailing newline from echo
	if(bytes>1 && string[bytes-1]=='\n'){
		string[bytes-1]=0;
	}

	if(strncmp(string, RESET_VIO_SOFT, strlen(RESET_VIO_SOFT))==0){
		printf("Client requested soft reset\n");
		pthread_mutex_lock(&mv_mtx);
		mvVISLAM_Reset(mv_vislam_ptr, 0);
		pthread_mutex_unlock(&mv_mtx);
		return;
	}
	if(strncmp(string, RESET_VIO_HARD, strlen(RESET_VIO_HARD))==0){
		printf("Client requested hard reset\n");
		// mvVISLAM_Reset is not reliable, just reinitialize the whole object instead
		// mvVISLAM_Reset(mv_vislam_ptr, 1);
		_hard_reset(); // close and restart the mvvislam object
		return;
	}
	if(strcmp(string, BLANK_VIO_CAM_COMMAND)==0){
		printf("Client requested image blank test\n");
		blank_counter = 90;
		return;
	}

	if(strcmp(string, FADE_VIO_CAM_COMMAND)==0){
		printf("Client requested image fade test\n");
		fade_counter = 255;
		return;
	}

	printf("WARNING: Server received unknown command through the control pipe!\n");
	printf("got %d bytes. Command is: %s\n", bytes, string);
	return;
}

static void _overlay_connect_cb(	__attribute__((unused))int ch, \
							__attribute__((unused))int client_id,
							char* client_name,\
							__attribute__((unused)) void* context)
{
	printf("client \"%s\" connected to overlay\n", client_name);
	return;
}

static void _overlay_disconnect_cb(	__attribute__((unused))int ch, \
							__attribute__((unused))int client_id,
							char* client_name,\
							__attribute__((unused)) void* context)
{
	printf("client \"%s\" disconnected from overlay\n", client_name);
	return;
}

static void _cpu_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("Connected to cpu-monitor\n");
	return;
}

static void _cpu_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "Disconnected from cpu-monitor\n");
	return;
}


// called whenever the simple helper has data for us to process
static void _cpu_helper_cb(__attribute__((unused))int ch, char* raw_data, int bytes, __attribute__((unused)) void* context)
{
	int n_packets;
	cpu_stats_t *data_array = modal_cpu_validate_pipe_data(raw_data, bytes, &n_packets);
	if (data_array == NULL) return;

	// only use most recent packet
	cpu_stats_t data = data_array[n_packets-1];

	if(data.flags&CPU_STATS_FLAG_STANDBY_ACTIVE){
		if(!standby_active){
			printf("Entering standby mode\n");
			standby_active = 1;
		}
	}
	else{
		if(standby_active){
			printf("Exiting standby mode\n");
			standby_active = 0;
		}
	}

	return;
}

// imu callback registered to the imu server
static void _imu_helper_cb(__attribute__((unused))int ch, char* data, int bytes,\
											__attribute__((unused)) void* context)
{
	if(en_debug_crash) fprintf(stderr, "0\n");
	// validate that the data makes sense
	int i, n_packets;
	imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_packets);

	// if there was an error OR no packets received, just return;
	if(data_array == NULL) return;
	if(n_packets<=0) return;

	// flag that imu data is active, skip data if camera is disconnected
	is_imu_connected = 1;
	global_error_codes &= ~ERROR_CODE_IMU_MISSING;
	if(!is_cam_connected) return;
	if(!is_initialized) return;
	pthread_mutex_lock(&mv_mtx);

	// time this in debug mode
	int64_t time_before, process_time;
	if(en_debug_timing_imu) time_before = _apps_time_monotonic_ns();
	if(en_debug_crash) fprintf(stderr, "1\n");

	// add all data into VIO
	for(i=0; i<n_packets; i++){

		// check if we somehow got an out-of-order imu sample and reject it
		if((int64_t)data_array[i].timestamp_ns <= last_imu_timestamp_ns){
			double dt = (last_imu_timestamp_ns - data_array[i].timestamp_ns)/1000000.0;
			fprintf(stderr, "WARNING out-of-order imu %fms before previous\n", dt);
			continue;
		}



		// pass data straight into mvvislam
		mvVISLAM_AddAccel(mv_vislam_ptr, data_array[i].timestamp_ns, (double)data_array[i].accl_ms2[0],
									(double)data_array[i].accl_ms2[1], (double)data_array[i].accl_ms2[2]);
		mvVISLAM_AddGyro(mv_vislam_ptr, data_array[i].timestamp_ns, (double)data_array[i].gyro_rad[0],
									(double)data_array[i].gyro_rad[1], (double)data_array[i].gyro_rad[2]);

		// record last timestamp so the camera thread is aware
		last_imu_timestamp_ns = data_array[i].timestamp_ns;
	}

	pthread_mutex_unlock(&mv_mtx);
	if(en_debug_crash) fprintf(stderr, "2\n");

	if(en_debug_timing_imu){
		process_time = _apps_time_monotonic_ns() - time_before;
		printf("IMU proc time %6.2fms for %d samples\n", ((double)process_time)/1000000.0, n_packets);
	}

	return;
}


#ifdef PLATFORM_QRB5165
// for qrb5165 only (right now) set the camera processing thread to run on 
// CPU 7 which is the fastest core
static void _check_and_set_affinity(void)
{
	// only do this once
	static int has_set = 0;
	if(has_set) return;

	cpu_set_t cpuset;
	pthread_t thread;
	thread = pthread_self();

	/* Set affinity mask to include CPUs 7 only */
	CPU_ZERO(&cpuset);
	CPU_SET(7, &cpuset);
	if(pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset)){
		perror("pthread_setaffinity_np");
	}

	/* Check the actual affinity mask assigned to the thread */
	if(pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset)){
		perror("pthread_getaffinity_np");
	}
	printf("Camera processing thread is now locked to the following cores:");
	for (int j = 0; j < CPU_SETSIZE; j++){
		if(CPU_ISSET(j, &cpuset)) printf(" %d", j);
	}
	printf("\n");

	// only do this once on start
	has_set = 1;

	return;
}
#endif


// camera frame callback registered to voxl-camera-server
static void _cam_helper_cb(__attribute__((unused))int ch, camera_image_metadata_t meta,\
							char* frame, __attribute__((unused)) void* context)
{
#ifdef PLATFORM_QRB5165
	_check_and_set_affinity();
#endif

	uint8_t* img;
	static int skipped_frames = 0;

	// check image format and grab correct img pointer from stereo if needed
	if(meta.format == IMAGE_FORMAT_RAW8 || meta.format == IMAGE_FORMAT_NV12){
		global_error_codes &= ~ERROR_CODE_CAM_BAD_FORMAT;
		img = (uint8_t*)frame;
	}
	else if(meta.format == IMAGE_FORMAT_STEREO_RAW8 || meta.format == IMAGE_FORMAT_STEREO_NV12){
		if(use_second_cam_if_stereo){
			img = (uint8_t*)frame + (meta.size_bytes/2);
		}
		img = (uint8_t*)frame;
		global_error_codes &= ~ERROR_CODE_CAM_BAD_FORMAT;
	}
	else{
		fprintf(stderr, "ERROR only support raw8 & nv12, mono & stereo right now\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_FORMAT;
		return;
	}

	// check resolution
	if(meta.width != (int16_t)mv_cam_conf.pixelWidth || meta.height != (int16_t)mv_cam_conf.pixelHeight){
		fprintf(stderr, "ERROR expected %dx%d img, got %dx%d\n",
					meta.width, meta.height, mv_cam_conf.pixelWidth, mv_cam_conf.pixelHeight);
		fprintf(stderr, "Image resolution must match what's in the selected lens cal file\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_RES;
		return;
	}
	else{
		global_error_codes &= ~ERROR_CODE_CAM_BAD_RES;
	}

	// Make the timestamp be the center of the exposure
	int64_t cam_timestamp_ns = meta.timestamp_ns;
	cam_timestamp_ns += meta.exposure_ns / 2;

	// TODO perhaps just flush the whole buffer instead of only old frames?
	// this keeps the fifo from overflowing
	if(cam_timestamp_ns < (_apps_time_monotonic_ns()-1000000000)){
		global_error_codes |= ERROR_CODE_DROPPED_CAM;
		pipe_client_flush(CAM_CH);
		fprintf(stderr, "ERROR detected frame older than 1s, flushing cam pipe\n");
		return;
	}

	// flag that camera data is active, skip frame is imu is disconnected
	is_cam_connected = 1;
	global_error_codes &= ~ERROR_CODE_CAM_MISSING;
	if(!is_imu_connected) return;
	if(!is_initialized) return;

	// skip frames in standby mode
	if(standby_active && skipped_frames < standby_skip_frames){
		skipped_frames ++;
		return;
	}
	skipped_frames = 0;


	// don't let image go in until IMU has caught up
	// if cam-imu alignment is POSITIVE that means the camera timestamp is early
	// and the image was actually taken after the reported timestamp
	while(last_imu_timestamp_ns < (cam_timestamp_ns + last_time_alignment_ns)){

		// don't get stuck here forever
		if(!main_running) return;
		if(!is_imu_connected) return;
		if(!is_initialized) return;
		if(cam_timestamp_ns < (_apps_time_monotonic_ns()-300000000)){
			global_error_codes |= ERROR_CODE_DROPPED_CAM;
			pipe_client_flush(CAM_CH);
			fprintf(stderr, "ERROR waited more than 0.3 seconds for imu to catch up, flushing camera pipe\n");
			return;
		}
		// small sleep if necessary
		if(en_debug){
			printf("waiting for imu\n");
		}
		usleep(5000);
	}


	// if fade counter is positive then fade out the image
	if(fade_counter>0){
		uint8_t tmp = 255 - fade_counter;
		for(int i=0; i<meta.size_bytes; i++){
			img[i]-=tmp;
			if(img[i]>fade_counter) img[i]=0; // fix wraparound
		}
		fade_counter-=2;
		if(fade_counter<=0) blank_counter = 120;
		if(en_debug) printf("fade out image, counter: %d\n", fade_counter);
	}

	// if blank counter is positive then blank out the image
	else if(blank_counter>0){
		memset(img, 0, meta.size_bytes);
		blank_counter--;
		if(en_debug) printf("blanked out image, counter: %d\n", blank_counter);
	}

	// don't continue if other things are still initializing
	if(!is_initialized) return;

	// lock the mv mutex, don't unlock until after getting the pose
	pthread_mutex_lock(&mv_mtx);
	if(en_debug_crash) fprintf(stderr, "3\n");
	int64_t time_before = _apps_time_monotonic_ns();
	mvVISLAM_AddImage(mv_vislam_ptr, cam_timestamp_ns , img);
	int64_t process_time = _apps_time_monotonic_ns() - time_before;
	if(en_debug_crash) fprintf(stderr, "4\n");

	// save details of the camera frame we just passed into mvvislam
	last_frame_frame_id = meta.frame_id;
	last_frame_timestamp_ns = meta.timestamp_ns;
	//printf("cam_timestamp_ns: %lld\n", cam_timestamp_ns);

	// record timestamp of camera frame AFTER it's been injested
	// this is the middle of the exposure, used for making sure imu data
	// is up to data before passing in next frame
	last_cam_timestamp_ns = cam_timestamp_ns;
	//pthread_mutex_unlock(&mv_mtx);

	if(process_time > 500000000){
		// if image processing took more than half a second, something went wrong
		// and we should drop frames
		global_error_codes |= ERROR_CODE_DROPPED_CAM;
		pipe_client_flush(CAM_CH);
		fprintf(stderr, "ERROR slow image proc time: %6.2fms\n", ((double)process_time)/1000000.0);
		fprintf(stderr, "Flushing camera frames\n");
	}
	else if(process_time > 50000000){
		// warn if image processing was a little slow, this happens from time
		// to time and isn't a big deal
		fprintf(stderr, "WARNING slow image proc time: %6.2fms\n", ((double)process_time)/1000000.0);
	}

	if(en_debug_timing_cam){
		printf("CAM proc time %6.2fms\n", ((double)process_time)/1000000.0);
	}


	_publish(meta, img);
	pthread_mutex_unlock(&mv_mtx);
	return;
}


#ifdef PLATFORM_QRB5165
	#include "slpi_gps_listener.h"
#endif

// return 0 if all is well, otherwise return the reason for blowup
static int _check_for_blowup(mvVISLAMPose p, int good_features)
{
	int64_t current_ts = p.time;
	static int64_t last_time_with_good_cov = 0;
	static int64_t last_time_with_enough_features = 0;

	// reset timers to current time after reset so we don't trip this during init
	if(hard_reset_blowup_flag){
		last_time_with_enough_features = current_ts;
		last_time_with_good_cov = current_ts;
		hard_reset_blowup_flag = 0;
	}

	// Now go through our 4 blowup criteria
	// max velocity check
	float vel = sqrtf(	(p.velocity[0]*p.velocity[0]) + \
						(p.velocity[1]*p.velocity[1]) + \
						(p.velocity[2]*p.velocity[2]));
	if(vel > auto_reset_max_velocity){
		fprintf(stderr, "WARNING auto-resetting due to exceeding max velocity of %4.1fm/s\n", (double)auto_reset_max_velocity);
		return ERROR_CODE_VEL_INST_CERT;
	}
	
	// get max velocity covariance
	float cov = p.errCovVelocity[0][0];
	if(p.errCovVelocity[1][1]>cov) cov = p.errCovVelocity[1][1];
	if(p.errCovVelocity[2][2]>cov) cov = p.errCovVelocity[2][2];

	// min feature timeout check
	if(good_features>auto_reset_min_features){
		last_time_with_enough_features = current_ts;
	}
	else{
		float tmp = (float)(current_ts - last_time_with_enough_features)/1000000000.0f;
		if(tmp>auto_reset_min_feature_timeout_s){
			fprintf(stderr, "WARNING auto-resetting due to low feature count\n");
			return ERROR_CODE_LOW_FEATURES;
		}
	}

	// max v cov timeout check
	if(cov<auto_reset_max_v_cov){
		last_time_with_good_cov = current_ts;
	}
	else{
		float tmp = (current_ts - last_time_with_good_cov)/1000000000.0f;
		if(tmp>auto_reset_max_v_cov_timeout_s){
			fprintf(stderr, "WARNING auto-resetting due to high vel covariance\n");
			return ERROR_CODE_VEL_WINDOW_CERT;
		}
	}

	// check for instant vel covariance limit
	if(cov  > auto_reset_max_v_cov_instant){
		fprintf(stderr, "WARNING auto-resetting due to vel covariance instant limit\n");
		return ERROR_CODE_VEL_INST_CERT;
	}

	// all is good (for now)
	return 0;
}



static void _publish(camera_image_metadata_t meta, uint8_t* img)
{
	mvVISLAMPose p;		// pose data from mvvislam
	ext_vio_data_t d;	// complete "extended" vio MPA packet
	vio_data_t s;		// simplified vio packet
	int nPoints;
	int n_good_points = 0;
	int n_oos_points = 0;
	int i,j;

	// make sure we start with clean data structs and apply any global error codes
	// full extended qvio packet, only needs memset as we memcpy the simple packet in later
	memset(&d,0,sizeof(d));
	// simple lib modal pipe standard vio packet
	memset(&s,0,sizeof(s));
	s.magic_number = VIO_MAGIC_NUMBER;
	s.error_code = global_error_codes;

	// Grab the pose and feature points
	if(en_debug_crash) fprintf(stderr, "7\n");
	int64_t time_before, process_time;
	if(en_debug_timing_pose) time_before = _apps_time_monotonic_ns();
	p = mvVISLAM_GetPose(mv_vislam_ptr);
	if(en_debug_crash) fprintf(stderr, "8\n");

	// get features and calculate the number of good features
	#define MAX_POINTS 256
	mvVISLAMMapPoint pPoints[MAX_POINTS];
	nPoints = mvVISLAM_GetPointCloud(mv_vislam_ptr, pPoints,MAX_POINTS);
	if(en_debug_timing_pose){
		process_time = _apps_time_monotonic_ns()-time_before;
		printf("getpose time = %5.2fms\n", process_time/1000000.0);
	}

	if(en_debug_crash) fprintf(stderr, "9\n");

	// record that we just got a successful pose and point cloud
	last_real_pose_timestamp_ns = p.time;

	for(i=0;i<nPoints;i++){
		if(pPoints[i].pointQuality==2 && pPoints[i].pixLoc[0]>0.0f && pPoints[i].pixLoc[1]>0.0f){
			n_good_points++;
		}
		if(pPoints[i].pointQuality == 1){
			n_oos_points++;
		}

		if (i < VIO_MAX_REPORTED_FEATURES){
			d.features[i].id = pPoints[i].id;
			d.features[i].cam_id = 0;
			memcpy(d.features[i].pix_loc, pPoints[i].pixLoc, sizeof(d.features[i].pix_loc));
			memcpy(d.features[i].tsf, pPoints[i].tsf, sizeof(d.features[i].tsf));
			memcpy(d.features[i].p_tsf, pPoints[i].p_tsf, sizeof(d.features[i].p_tsf));
			d.features[i].depth = pPoints[i].depth;
			d.features[i].depth_error_stddev = pPoints[i].depthErrorStdDev;
			d.features[i].point_quality = (vio_point_quality_t)pPoints[i].pointQuality;
			//printf("%2d %7.1f %7.1f %7.1f %7.1f\n", n_good_points, (double)pPoints[i].depth, (double)pPoints[i].depthErrorStdDev, d.features[i].pix_loc[0], d.features[i].pix_loc[1]);

		}
	}

	// limit the number of features to what fits in our pipe packet
	d.n_total_features = nPoints;
	if(d.n_total_features > VIO_MAX_REPORTED_FEATURES){
		d.n_total_features = VIO_MAX_REPORTED_FEATURES;
	}

	//if(en_debug) printf("total points: %3d  Good Points: %3d\n", nPoints, n_good_points);

	// grab state from mv
	if(p.poseQuality == MV_TRACKING_STATE_FAILED) s.state = VIO_STATE_FAILED;
	else if(p.poseQuality == MV_TRACKING_STATE_INITIALIZING) s.state = VIO_STATE_INITIALIZING;
	else s.state = VIO_STATE_OK;

	// sometimes qvio will report covariance as invalid but state is still OKAY
	// this is NOT alright, in this case manually set the state to failed.
	if(p.errCovPose[3][3]<=0.0f || p.errCovPose[4][4]<=0.0f || p.errCovPose[5][5]<=0.0f){
		s.state=VIO_STATE_FAILED;
	}

	// we finished initializing, no longer check for init timeout
	if(s.state == VIO_STATE_OK){
		init_failure_detector_reset_flag = 0;
	}

	// if we just went from good to failed, treat this like a reset for the
	// init failure detector so it can timeout the same as VIO tries to re-init
	// itself after it's own internal reset
	if(last_state!=VIO_STATE_FAILED && s.state==VIO_STATE_FAILED){
		init_failure_detector_reset_flag = 1;
		time_of_last_reset = p.time;
	}

	// record time when vio claimed to have initialized and only check for
	// for blowups some time after this
	if(last_state!=VIO_STATE_OK && s.state==VIO_STATE_OK && en_auto_reset){
		blowup_detector_flag = 1;
		time_of_first_okay = p.time;
	}
	last_state = s.state;

	// while VIO state is OK, do our own additional blowup checks if enough
	// time has passed since the init
	int64_t time_since_first_okay = _apps_time_monotonic_ns() - time_of_first_okay;
	if(blowup_detector_flag && time_since_first_okay>BLOWUP_DETECT_TIMEOUT_NS){
		int code = _check_for_blowup(p,n_good_points);
		if(code){
			pthread_mutex_unlock(&mv_mtx);
			_hard_reset();
			s.state = VIO_STATE_FAILED;
			s.error_code |= code;
		}
	}


	// don't send packets from the past, this can happen when qvio stalls
	// during a reset
	if(p.time<last_sent_timestamp_ns){
		//fprintf(stderr, "WARNING skipping pose data from the past\n");
		return;
	}

	// All checks passed, after this point this function should not return
	// until the end
	last_sent_timestamp_ns = p.time;


	// error code is a bitmask that we might have added to already so OR
	// it with VIOs error code
	s.error_code |= p.errorCode;

	// turn off dropped imu error for now, I think this error code is being
	// reported incorrectly by VIO since it's sportadic and seems to have no
	// correlation with IMU rate or phase with camera.
	s.error_code &= ~ERROR_CODE_DROPPED_IMU;

	// populate some other data
	s.timestamp_ns = p.time;
	s.n_feature_points = n_good_points;

	d.imu_cam_time_shift_s = p.timeAlignment;
	last_time_alignment_ns = p.timeAlignment * 1000000000;
	d.last_cam_frame_id = last_frame_frame_id;
	d.last_cam_timestamp_ns = last_frame_timestamp_ns;

	// deconstruct mv's 6DRT struct
	for(i=0;i<3;i++){
		s.T_imu_wrt_vio[i] = p.bodyPose.matrix[i][3];
		for(j=0;j<3;j++) s.R_imu_to_vio[i][j] = p.bodyPose.matrix[i][j];
	}

	// pose covariance diagonals, 6 entries
	s.pose_covariance[0] =  (float)p.errCovPose[0][0];
	s.pose_covariance[6] =  (float)p.errCovPose[1][1];
	s.pose_covariance[11] = (float)p.errCovPose[2][2];
	s.pose_covariance[15] = (float)p.errCovPose[3][3];
	s.pose_covariance[18] = (float)p.errCovPose[4][4];
	s.pose_covariance[20] = (float)p.errCovPose[5][5];

	// velocity covariance diagonals, 3 entries
	s.velocity_covariance[0] = p.errCovVelocity[0][0];
	s.velocity_covariance[6] = p.errCovVelocity[1][1];
	s.velocity_covariance[11] = p.errCovVelocity[2][2];

	// memcpy the rest
	memcpy(s.T_cam_wrt_imu,		p.tbc,				sizeof(float)*3);
	memcpy(s.R_cam_to_imu,		p.Rbc,				sizeof(float)*3*3);
	memcpy(s.vel_imu_wrt_vio,	p.velocity,			sizeof(float)*3);
	memcpy(s.imu_angular_vel,	p.angularVelocity,	sizeof(float)*3);
	memcpy(s.gravity_vector,	p.gravity,			sizeof(float)*3);
	
	memcpy(d.gravity_covariance,p.errCovGravity,	sizeof(float)*3*3);
	memcpy(d.gyro_bias,			p.wBias,			sizeof(float)*3);
	memcpy(d.accl_bias,			p.aBias,			sizeof(float)*3);

	// calculate the quality
	s.quality = calc_quality(s.state, s.velocity_covariance, mv_cam_conf.pixelWidth,\
					mv_cam_conf.pixelHeight, d.n_total_features, d.features);

	// fill the simplified struct in our extended packet
	memcpy(&d.v, &s, sizeof(vio_data_t));

	// send to both pipes
	pipe_server_write(EXTENDED_CH, (char*)&d, sizeof(ext_vio_data_t));
	pipe_server_write(SIMPLE_CH,   (char*)&s, sizeof(vio_data_t));

	// for debug only
	if(en_debug){
		printf("state: ");
		pipe_print_vio_state(s.state);
		printf(" err: ");
		pipe_print_vio_error(s.error_code);
		printf("\n");
	}
	if(en_debug_pos){
		printf("%6.3f %6.3f %6.3f ", (double)s.T_imu_wrt_vio[0],(double)s.T_imu_wrt_vio[1],(double)s.T_imu_wrt_vio[2]);
		printf("\n");
	}

	// turn off dropped frame code now we have informed everyone.
	global_error_codes &= ~ERROR_CODE_DROPPED_CAM;

	// if someone has subscribed to the overlay, draw it
	if(pipe_server_get_num_clients(OVERLAY_CH) > 0){

		if(en_debug_crash) fprintf(stderr, "10\n");
		_draw_and_publish_vio_overlay(OVERLAY_CH, &meta, (char*)img, d);
		if(en_debug_crash) fprintf(stderr, "11\n");
	}

	return;

}



// called whenever we connect or reconnect to the imu server
// don't flag as connected yet, not until we get data
static void _imu_connect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	printf("connected to imu server\n");
	pipe_client_set_pipe_size(IMU_CH, IMU_PIPE_MIN_PIPE_SIZE);
	return;
}

// called whenever we disconnect from the imu server
static void _imu_disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "WARNING: disconnected from imu server, resetting VIO\n");
	global_error_codes |= ERROR_CODE_IMU_MISSING;
	last_imu_timestamp_ns = 0;
	is_imu_connected = 0;
	_hard_reset();
	return;
}

// called whenever we connect or reconnect to the imu server
// don't flag as connected yet, not until we get data
static void _cam_connect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	printf("connected to camera server\n");
	pipe_client_set_pipe_size(CAM_CH, CAM_PIPE_SIZE);
	return;
}

// called whenever we disconnect from the camera server
static void _cam_disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "WARNING: disconnected from camera server, resetting VIO\n");
	global_error_codes |= ERROR_CODE_CAM_MISSING;
	last_cam_timestamp_ns = 0;
	is_cam_connected = 0;
	_hard_reset();
	return;
}

static int load_cam_cal()
{
	printf("loading %s\n", lens_cal_path);
	cJSON* json = json_from_yaml(lens_cal_path);
	if(json==NULL){
		return -1;
	}

	cJSON* M = NULL;
	cJSON* D = NULL;
	double Mdata[9];
	double Ddata[9];
	int len = 0;
	int mode = 0; // 0=failed, 1=mono, 2=stereo first, 3=stereo second
	int mv_model, n_coef;

	// first look for M which should be present for a mono camera
	// if it's missing, try M1 or M2 for stereo cam files
	M = cJSON_GetObjectItem(json, "M");
	if(M != NULL){
		mode = 1;
	}
	else{
		if(!use_second_cam_if_stereo){
			M = cJSON_GetObjectItem(json, "M1");
			if(M != NULL) mode = 2;
		}
		else{
			M = cJSON_GetObjectItem(json, "M2");
			if(M != NULL) mode = 3;
		}
	}
	if(mode==0){
		fprintf(stderr, "WARNING failed to find field 'M' in camera cal file\n");
		return -1;
	}

	// now grab the correct distortion vector object by mode
	if(mode==1)      D = json_fetch_object(json, "D");
	else if(mode==2) D = json_fetch_object(json, "D1");
	else             D = json_fetch_object(json, "D2");

	if(D==NULL){
		fprintf(stderr, "WARNING failed to find field 'D' in camera cal file\n");
		return -1;
	}

	// check if we are a fisheye lens or not
	char model[128];
	if(json_fetch_string(json, "distortion_model", model, 127)){
		fprintf(stderr, "WARNING failed to find distortion_model in camera cal file\n");
		return -1;
	}
	if(strcmp(model,"fisheye")==0){
		mv_model = 10;
		n_coef = 4;
	}
	else{
		mv_model = 5;
		n_coef = 5;
	}

	// pick the data out of each M and D objects
	if(json_fetch_dynamic_vector(M, "data", Mdata, &len, 9)){
		fprintf(stderr, "WARNING failed to find M 'data' in camera cal file\n");
		return -1;
	}
	if(len != 9){
		fprintf(stderr, "WARNING M 'data' field in camera cal file should be of length 9\n");
		return -1;
	}

	if(json_fetch_dynamic_vector(D, "data", Ddata, &len, 9)){
		fprintf(stderr, "WARNING failed to find D 'data' in camera cal file\n");
		return -1;
	}
	if(len < n_coef){
		fprintf(stderr, "WARNING D 'data' field in camera cal file should be of length >= %d\n", n_coef);
		return -1;
	}

	// pick out height and width
	int width, height;
	if(json_fetch_int(json, "width", &width)){
		fprintf(stderr, "WARNING failed to find width in camera cal file\n");
		return -1;
	}
	if(json_fetch_int(json, "height", &height)){
		fprintf(stderr, "WARNING failed to find height in camera cal file\n");
		return -1;
	}


	// all data read from file, apply it to the mv structure
	mv_cam_conf.pixelWidth			= width;
	mv_cam_conf.pixelHeight			= height;
	mv_cam_conf.memoryStride		= width;
	mv_cam_conf.focalLength[0]		= Mdata[0];
	mv_cam_conf.focalLength[1]		= Mdata[4];
	mv_cam_conf.principalPoint[0]	= Mdata[2];
	mv_cam_conf.principalPoint[1]	= Mdata[5];
	mv_cam_conf.distortionModel		= mv_model;
	for(int i=0; i<n_coef; i++){
		mv_cam_conf.distortion[i] = Ddata[i];
	}

	cJSON_Delete(json);
	return 0;
}


// main just reads config files, opens pipes, and waits
int main(int argc, char* argv[])
{
	int i, n;

	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	////////////////////////////////////////////////////////////////////////////////
	// load config
	////////////////////////////////////////////////////////////////////////////////

	// start with the qvio config file as it contains imu/camera names
	printf("loading config file\n");
	if(config_file_read()) return -1;
	// in config only mode, just quit now
	if(en_config_only) return 0;
	// in normal mode print the config for logging purposes
	config_file_print();

	////////////////////////////////////////////////////////////////////////////////
	// gracefully handle an existing instance of the process and associated PID file
	////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

////////////////////////////////////////////////////////////////////////////////
// set this critical process to use FIFO scheduler with high priority
////////////////////////////////////////////////////////////////////////////////

	struct sched_param param;
	memset(&param, 0, sizeof(sched_param));
	param.sched_priority = 95;
	fprintf(stderr, "setting scheduler\n");
	int ret = sched_setscheduler(0, SCHED_FIFO, &param);
	if(ret==-1){
		fprintf(stderr, "WARNING Failed to set priority, errno = %d\n", errno);
		fprintf(stderr, "This seems to be a problem with ADB, the scheduler\n");
		fprintf(stderr, "should work properly when this is a background process\n");
	}
	// check
	ret = sched_getscheduler(0);
	if(ret!=SCHED_FIFO){
		fprintf(stderr, "WARNING: failed to set scheduler\n");
	}
	else{
		// even thought this is a success, print to stderr to that it shows up
		// in the correct order. stdout logs in journalctl are usually out of
		// sync with stderr
		fprintf(stderr, "INFO: set FIFO priority successfully!\n");
	}

	// The threads created by libmodal_pipe after this should inherit this
	// priority, TODO validate this


////////////////////////////////////////////////////////////////////////////////
// setup
////////////////////////////////////////////////////////////////////////////////

	// starting point for cam alignment comes from config file
	last_time_alignment_ns = cam_imu_timeshift_s * 1000000000;

	// now grab the extrinsic relation between the imu and camera specified in
	// the qvio config file
	printf("loading extrinsics config file\n");
	vcc_extrinsic_t ext[VCC_MAX_EXTRINSICS_IN_CONFIG];
	vcc_extrinsic_t cam_wrt_imu_ext;
	if(vcc_read_extrinsic_conf_file(VCC_EXTRINSICS_PATH, ext, &n, VCC_MAX_EXTRINSICS_IN_CONFIG)) _quit(-1);
	if(vcc_find_extrinsic_in_array(imu_name, cam_extrinsics_name, ext, n, &cam_wrt_imu_ext)){
		fprintf(stderr, "failed to find extrinsics from %s to %s in %s\n", imu_name, cam_extrinsics_name, VCC_EXTRINSICS_PATH);
		_quit(-1);
	}
	vcc_print_extrinsic_conf(&cam_wrt_imu_ext, 1);

	for(i=0;i<3;i++){
		tbc[i] = cam_wrt_imu_ext.T_child_wrt_parent[i];
	}
	_tait_bryan_xyz_intrinsic_to_axis_angle(cam_wrt_imu_ext.RPY_parent_to_child, ombc);


	printf("tbc:  %6.3f %6.3f %6.3f\n", (double)tbc[0], (double)tbc[1], (double)tbc[2]);
	printf("ombc: %6.3f %6.3f %6.3f (axis angle)\n", (double)ombc[0], (double)ombc[1], (double)ombc[2]);
	printf("ombc: %6.3f %6.3f %6.3f (RPY deg)\n", \
							(double)cam_wrt_imu_ext.RPY_parent_to_child[0],\
							(double)cam_wrt_imu_ext.RPY_parent_to_child[1],\
							(double)cam_wrt_imu_ext.RPY_parent_to_child[2]);


	// these are default intrinsic lens constants for ModalAI Tracking camera
	mv_cam_conf.pixelWidth			= 640;
	mv_cam_conf.pixelHeight			= 480;
	mv_cam_conf.memoryStride		= 640;
	mv_cam_conf.uvOffset			= 0;
	mv_cam_conf.principalPoint[0]	= 319.625;
	mv_cam_conf.principalPoint[1]	= 243.144;
	mv_cam_conf.focalLength[0]		= 275.078;
	mv_cam_conf.focalLength[1]		= 274.931;
	mv_cam_conf.distortion[0]		= 0.003908;
	mv_cam_conf.distortion[1]		= -0.009574;
	mv_cam_conf.distortion[2]		= 0.010173;
	mv_cam_conf.distortion[3]		= -0.003329;
	mv_cam_conf.distortion[4]		= 0;
	mv_cam_conf.distortion[5]		= 0;
	mv_cam_conf.distortion[6]		= 0;
	mv_cam_conf.distortion[7]		= 0;
	mv_cam_conf.distortionModel		= 10; // fisheye

	if(load_cam_cal()){
		fprintf(stderr, "\nWARNING: Failed to open lens calibration file:\n");
		fprintf(stderr, "%s\n", lens_cal_path);
		fprintf(stderr, "using default tracking camera intrinsics for now\n");
		fprintf(stderr, "follow instructions here to calibrate the tracking cam:\n");
		fprintf(stderr, "https://docs.modalai.com/calibrate-cameras/\n\n");
	}

	if(mv_cam_conf.distortionModel == 10){
		printf("using fisheye camera intrinsics:\n");
		printf("distortion: %f %f %f %f\n", mv_cam_conf.distortion[0],mv_cam_conf.distortion[1],mv_cam_conf.distortion[2],mv_cam_conf.distortion[3]);
	}
	else{
		printf("using pin-hole camera intrinsics:\n");
		printf("distortion: %f %f %f %f %f\n", mv_cam_conf.distortion[0],mv_cam_conf.distortion[1],mv_cam_conf.distortion[2],mv_cam_conf.distortion[3],mv_cam_conf.distortion[4]);
	}
	printf("focal lengths: %f %f\n", mv_cam_conf.focalLength[0], mv_cam_conf.focalLength[1]);
	printf("principle points: %f %f\n", mv_cam_conf.principalPoint[0], mv_cam_conf.principalPoint[1]);

	// start VIO with the reset function
	_hard_reset();


////////////////////////////////////////////////////////////////////////////////
// start the server pipe and data thread to write to it
////////////////////////////////////////////////////////////////////////////////

	int flags = SERVER_FLAG_EN_CONTROL_PIPE;

	// init extended pipe
	pipe_info_t info1 = { \
		QVIO_EXTENDED_NAME,			// name
		QVIO_EXTENDED_LOCATION,		// location
		"ext_vio_data_t",				// type
		PROCESS_NAME,				// server_name
		VIO_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(EXTENDED_CH, info1, flags)){
		_quit(-1);
	}

	// add in optional fields to the info JSON file
	cJSON* json = pipe_server_get_info_json_ptr(EXTENDED_CH);
	cJSON_AddStringToObject(json, "imu", imu_name);
	cJSON_AddStringToObject(json, "cam", cam_name);
	pipe_server_update_info(EXTENDED_CH);
	pipe_server_set_control_cb(EXTENDED_CH, _control_pipe_cb, NULL);
	pipe_server_set_available_control_commands(EXTENDED_CH, QVIO_CONTROL_COMMANDS);


	// init simple pipe
	pipe_info_t info2 = { \
		QVIO_SIMPLE_NAME,			// name
		QVIO_SIMPLE_LOCATION,		// location
		"vio_data_t",				// type
		PROCESS_NAME,				// server_name
		VIO_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(SIMPLE_CH, info2, flags)){
		_quit(-1);
	}

	// add in optional fields to the info JSON file
	json = pipe_server_get_info_json_ptr(SIMPLE_CH);
	cJSON_AddStringToObject(json, "imu", imu_name);
	cJSON_AddStringToObject(json, "cam", cam_name);
	pipe_server_update_info(SIMPLE_CH);
	pipe_server_set_control_cb(SIMPLE_CH, _control_pipe_cb, NULL);
	pipe_server_set_available_control_commands(SIMPLE_CH, QVIO_CONTROL_COMMANDS);


	// init overlay pipe
	pipe_info_t info3 = { \
		QVIO_OVERLAY_NAME,			// name
		QVIO_OVERLAY_LOCATION,		// location
		"camera_image_metadata_t",		// type
		PROCESS_NAME,				// server_name
		CAM_PIPE_SIZE,				// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(OVERLAY_CH, info3, flags)){
		_quit(-1);
	}

	pipe_server_set_connect_cb(OVERLAY_CH, _overlay_connect_cb, NULL);
	pipe_server_set_disconnect_cb(OVERLAY_CH, _overlay_disconnect_cb, NULL);
	pipe_server_set_control_cb(OVERLAY_CH, _control_pipe_cb, NULL);
	pipe_server_set_available_control_commands(OVERLAY_CH, QVIO_CONTROL_COMMANDS);


	// indicate to the soon-to-be-started thread that we are initialized
	// and running, this is an extern variable in start_stop.c
	main_running = 1;

	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	pthread_create(&health_thread, &tattr, _health_thread_func, NULL);

////////////////////////////////////////////////////////////////////////////////
// now subscribe to cam and imu servers, waiting if they are not alive yet
// VIO needs IMU before camera so init in that order.
////////////////////////////////////////////////////////////////////////////////

	// until they connect, inidcate that they are disconnected
	global_error_codes |= ERROR_CODE_CAM_MISSING;
	global_error_codes |= ERROR_CODE_IMU_MISSING;

	// try to open a pipe to the imu server
	pipe_client_set_connect_cb(IMU_CH, _imu_connect_cb, NULL);
	pipe_client_set_disconnect_cb(IMU_CH, _imu_disconnect_cb, NULL);
	pipe_client_set_simple_helper_cb(IMU_CH, _imu_helper_cb, NULL);
	printf("waiting for imu\n");
	ret = pipe_client_open(IMU_CH, imu_name, PROCESS_NAME, \
			CLIENT_FLAG_EN_SIMPLE_HELPER, \
			IMU_RECOMMENDED_READ_BUF_SIZE);
	// check for error
	if(ret<0){
		fprintf(stderr, "Failed to open IMU pipe\n");
		pipe_print_error(ret);
		_quit(-1);
	}

	// try to open pipe to camera server
	pipe_client_set_connect_cb(CAM_CH, _cam_connect_cb, NULL);
	pipe_client_set_disconnect_cb(CAM_CH, _cam_disconnect_cb, NULL);
	pipe_client_set_camera_helper_cb(CAM_CH, _cam_helper_cb, NULL);
	printf("waiting for cam\n");
	ret = pipe_client_open(CAM_CH, cam_name, PROCESS_NAME, \
				CLIENT_FLAG_EN_CAMERA_HELPER, 0);
	// check for error
	if(ret<0){
		fprintf(stderr, "Failed to open CAM pipe\n");
		pipe_print_error(ret);
		_quit(-1);
	}

	// open optional GPS pipe
#ifdef PLATFORM_QRB5165
	if(enable_gps_vel){
		_slpi_gps_reader_init();
	}
#endif

	// open optional cpu monitor pipe
	if(en_standby_mode){
		pipe_client_set_connect_cb(CPU_CH, _cpu_connect_cb, NULL);
		pipe_client_set_disconnect_cb(CPU_CH, _cpu_disconnect_cb, NULL);
		pipe_client_set_simple_helper_cb(CPU_CH, _cpu_helper_cb, NULL);
		printf("waiting for cpu_monitor\n");
		ret = pipe_client_open(CPU_CH, "cpu_monitor", PROCESS_NAME, \
				CLIENT_FLAG_EN_SIMPLE_HELPER, CPU_STATS_RECOMMENDED_READ_BUF_SIZE);
		// check for error
		if(ret<0){
			fprintf(stderr, "Failed to open CPU pipe\n");
			pipe_print_error(ret);
			_quit(-1);
		}
	}

	// run until start/stop module catches a signal and changes main_running to 0
	while(main_running) usleep(5000000);


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	printf("joining health thread\n");
	pthread_join(health_thread, NULL);

	#ifdef PLATFORM_QRB5165
	if(enable_gps_vel){
		_slpi_gps_reader_stop();
	}
	#endif
	_quit(0);
	return 0;
}
